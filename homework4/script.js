// При використанні AJAX браузер взаємодіє з сервером асинхронно, тобто відправляє запити та отримує відповіді з сервера не перезавантажуючи сторінку

let API = "https://ajax.test-danit.com/api/swapi/"

function getRequest(url) {
    return fetch(url)
}

function getFilms() {
    getRequest(`${API}films`)
        .then(response => response.json())
        .then(data => {
            console.log(data);
            const list = document.createElement('ul')
            document.body.prepend(list);
            data.forEach(({ name, id, episodeId, openingCrawl }) => {
                list.insertAdjacentHTML("beforeend", `
                    <li data-id=${id}>
                      <header>
                        <p>Episode: ${episodeId}</p>
                        <p>${name}</p>
                      </header>
                      <div>${openingCrawl}</div>
                      <div class="actor_block">Actors: Loading...</div>
                    </li>
                    `)

                const actorBlock = document.querySelector(`[data-id="${id}"] .actor_block`);
                getActors(id).then((actors) => {
                    const actorList = actors.join(", ")
                    actorBlock.innerHTML = `Actors: ${actorList}`
                })

            })
        })
        .catch((error) => {
            console.log(error);
        })
}
function getActors(filmId) {
    return getRequest(`${API}films/${filmId}`)
        .then(response => response.json())
        .then(data => {
            const actorsResponse = data.characters.map(url => fetch(url).then(response => response.json()));
            return Promise.all(actorsResponse)
                .then(actorsData => actorsData.map(actor => actor.name));
        })
        .catch(error => {
            console.log(error);
        });
}

getFilms()

