const API = "https://ajax.test-danit.com/api/json/";

const list = document.querySelector("#list")

async function getRequest(url, method = "GET", confiq) {
    const request = await fetch(url, { method: method, ...confiq });
    return request;
}

class Card {
    constructor({ title, body, name, username, email, id, userId }, container) {
        this.title = title;
        this.body = body;
        this.name = name;
        this.username = username;
        this.email = email;
        this.id = id;
        this.userId = userId;
        this.container = container;
    }
    render() {
        this.container.insertAdjacentHTML("beforeend", `
       <div class="card w-75 mb-3" data-postid = ${this.id}>
       <div class="card-body">
         <h5 class="card-title">${this.name}  ${this.username}  ${this.email} <span style="cursor:pointer"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3" viewBox="0 0 16 16">
         <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5M11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47M8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5"/>
       </svg></span></h5>
         <h6 class="card-title">${this.title}</h6>
         <p class="card-text">${this.body}</p>
         
       </div>
     </div>

       `)

    }
}

async function getUsers() {
    const usersr = await getRequest(`${API}users`);
    const users = await usersr.json()
    return users
}

// getUsers();

async function getPosts(id) {
    const postsr = await getRequest(`${API}posts`);
    const posts = await postsr.json();
    const postsUser = await posts.filter((post) => post.userId === id)
    return postsUser
}

// getPosts(1);

async function deletePost(postId) {
   const userDel = await getRequest(`${API}/posts/${postId}`, "DELETE");
   console.log(userDel);
   if (userDel.ok) {
    alert(`post № ${postId} had been deleted`);
   }
}
// deletePost(3)

async function main() {
    const users = await getUsers();
    
    users.forEach((user) => {
        getPosts(user.id).then(data => {
            data.forEach((post) => {
                const userCard = new Card({ ...user, ...post }, list);
                userCard.render();
            })

        })
    });
    
}

main()

list.addEventListener("click", (e) => {
    
    if (!e.target.closest("span")) return
    console.log(e.target.closest('.card'));
    const id = e.target.closest('.card').dataset.postid;
    try {
        deletePost(id);
        e.target.closest('.card').remove()
    } catch (error) {
        console.log(error);
    }
    
    
})

