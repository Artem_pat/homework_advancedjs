const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

  // Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
  // Наприклад, буває таке що з сервера приходять дані не вірні, і для того щоб код не зупинився використовують конструкцію try...catch, 
  // щоб код продовжував виконуватися далі. При читанні або записі до файлів можуть виникати помилки, такі як відсутність файлу, неправильний шлях...

  
  const rootDiv = document.querySelector("#root");
  const listBooks = document.createElement('ul');
  rootDiv.appendChild(listBooks);

  function isValidBook(book) {
    try {
        if (!("name" in book)) {
            throw new Error("У книги відсутня назва")
        }
        if (!("author" in book)) {
            throw new Error("У книги відсутній автор")
        }
        if (!("price" in book)) {
            throw new Error("У книги відсутня ціна")
        }
        return true;
    } catch (error) {
        
        console.log("Помилка!: ", error.message);
        return false;
    }
    
  }

  books.forEach((book) => {
    if (isValidBook(book)) {
        const li = document.createElement('li');
        li.textContent = `${book.name}, ${book.author}, ${book.price}`;
        listBooks.appendChild(li);
    }
    
  } )
  
