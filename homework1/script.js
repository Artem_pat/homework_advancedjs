// 1) Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript.
// Прототипне наслідуванн, означає що у кожного об'єкта є прототип, тобто він наслідує деякі властивості і методи прототипу.
// 2) Для чого потрібно викликати super() у конструкторі класу-нащадка?
// Для того щоб ініціалізувати властивості класу, тобто добавить в конструктор розширеного класа нащадка, властивості батьківського класу. 


class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    set name(value) {
        this._name = value;
    }
    get name() {
        return this._name;
    }

    set age(value) {
        this._age = value;
    }
    get age() {
        return this._age;
    }

    set salary(value) {
        this._salary = value
    }
    get salary() {
        return this._salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this.lang = lang;
    }

    set salary(value) {
        this._salary = value * 3;
    }
    get salary() {
        return this._salary;
    }
}

const programmer1 = new Programmer();
programmer1.name = "John";
programmer1.age = 25;
programmer1.salary = 2000;
programmer1.lang = 'en';
console.log(programmer1);

const programmer2 = new Programmer();
programmer2.name = "Anna";
programmer2.age = 23;
programmer2.salary = 1500;
programmer2.lang = 'ukr';
console.log(programmer2);