const btn = document.querySelector("#find-ip");
console.log(btn);
const API = "http://ip-api.com/json/"

async function getRequest(url, method = "GET", confiq) {
    const request = await fetch(url, { method: method, ...confiq });
    return request;
}


btn.addEventListener("click", async () => {
    const requestIp = await getRequest('https://api.ipify.org/?format=json');
    const dataIp = await requestIp.json();
    console.log(dataIp);
    const requestLocation = await getRequest(`${API}${dataIp.ip}`);
    const dataLocation = await requestLocation.json();
    console.log(dataLocation);
    const div = document.createElement("div");
    document.body.append(div);
    div.insertAdjacentHTML("beforeend", `
        <p>country: ${dataLocation.country}</p>
        <p>city:${dataLocation.city}</p>
        <p>region:${dataLocation.region}</p>
        <p>region name: ${dataLocation.regionName}</p>

    `)
})